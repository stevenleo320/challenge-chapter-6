const express = require("express");
const logger = require("pino-http");
const dotenv = require("dotenv");

const route = require("./route");
const webRoute = require("./web-route");

const server = (app) => {
    dotenv.config();

    app.set("view engine","ejs");
    // Middleware logging
    app.use(logger());

    // Buat mengakses folder public tanpa harus /public 
    app.use(express.static("public"));
    app.use(express.static("assets"));

    // Karena return nya json maka pake express.json
    // Urlencode merupakan Payload handler -> jadi kalo submit form buat save data  di memory
    app.use(express.json());
    app.use(express.urlencoded( {extended : true} ));

    app.use(webRoute);
    app.use("/api/v1",route);

    return app;
};

module.exports = server;