'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGameHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      UserGameHistory.belongsTo(models.UserGame, {
        foreignKey: "userId"
      });

      UserGameHistory.belongsTo(models.UserGameBiodata, {
        foreignKey: "biodataId"
      });
    }
  }
  UserGameHistory.init({
    game_name: DataTypes.STRING,
    playing_time: DataTypes.INTEGER,
    score: DataTypes.INTEGER,
    userId: DataTypes.INTEGER,
    biodataId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UserGameHistory',
  });
  return UserGameHistory;
};