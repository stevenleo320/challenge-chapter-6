'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    
    await queryInterface.bulkInsert('UserGames', [
      {
        username: "StevenLeo",
        password: "test123",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
    
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('UserGames', { username: "StevenLeo", password: "test123"});
  }
};
