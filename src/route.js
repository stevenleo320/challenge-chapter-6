const express = require("express");
const bcrypt = require("bcrypt");
const { addHours, addMinutes } = require("date-fns");

const { Router } = require("express");
const route = express.Router();

const { UserGame, UserGameBiodata, UserGameHistory } = require("./models");
// , UserGameBiodata, UserGameHistory

// LOGIN USER GAME
route.post("/register/admin", async(req,res) => {
  try {
    const { body } = req;

    if(body.username && body.password) {
      const i_admin = await UserGame.create(body);
    }

    res.json(i_admin);

  } catch (error) {
    res.json("Username and Password must be required");
  }
  
});

route.get("/admin", async(req,res) => {
  const usergames = await UserGame.findAll({
    include: [
      {
          model: UserGameBiodata,
          attributes: ["name","email","country"]
      },
      {
          model: UserGameHistory,
          attributes: ["game_name","playing_time","score"]
      }
    ]
  });

  if(usergames) {
    res.json(usergames);
  } else {
    res.json("No Data !");
  }

});

// VIEW BASE ON ID
route.get("/admin/:id", async(req,res) => {
  try {
    const usergames = await UserGame.findAll({
      where: {
        id: req.params.id
      },
      include: [
        {
            model: UserGameBiodata,
            attributes: ["name","email","country"]
        },
        {
            model: UserGameHistory,
            attributes: ["game_name","playing_time","score"]
        }
      ]
    });

    res.json(usergames);

  } catch (error) {
    res.json("No Data !");
  }

});

// UPDATE USER GAME
route.put("/admin/:id", async (req,res) => {
  try {
    const { body } = req;

    const u_admin = await UserGame.update(body, {
      where: {
          id: req.params.id
      }
    });
    
    res.json(u_admin);
  
  } catch (error) {
    res.json("No Data Found!");
  }

});

// DELETE USER GAMES
route.delete("/admin/:id", async(req,res) => {
  try {
    const d_admin = await UserGame.destroy({
        where: { 
            id: req.params.id
        }
    });

    res.json(d_admin);

  } catch (error) { 
    res.json("Internal Error Server");
  }
});

// USER GAME BIODATA
// INSERT
route.post("/biodata/new", async(req,res) => {
  try {
    const { body } = req;
    
    const i_biodata = await UserGameBiodata.create(body);
  
    res.json(i_biodata);

  } catch (error){ 
    res.json("Internal Error Server");
  }
});

// VIEW DATA
route.get("/biodata/user", async(req,res) => {
  try {
    const userbiodata = await UserGameBiodata.findAll();
  
    res.json(userbiodata);

  } catch (error){ 
    res.json("Internal Error Server");
  }
  
});

// UPDATE
route.put("/biodata/:id", async (req,res) => {
  try {
    const { body } = req;
  
    const u_biodata = await UserGameBiodata.update(body, {
      where: {
          id: req.params.id
      }
    });

    res.json(u_biodata);
    
  } catch (error){ 
    res.json("Internal Error Server");
  }
});


// DELETE
route.delete("/biodata/:id", async(req,res) => {
  try {
    const d_biodata = await UserGameBiodata.destroy({
        where: { 
            id: req.params.id
        }
    });
    res.json(d_biodata);

  } catch (error){ 
    res.json("Internal Error Server");
  }
});

// USER GAME HISTORY
// INSERT
route.post("/history/new", async(req,res) => {
  try {
    const { body } = req;
  
    const i_biodata = await UserGameHistory.create(body);
  
    res.json(i_biodata);

  } catch (error){ 
    res.json("Internal Error Server");
  }
});

// VIEW DATA
route.get("/history/user", async(req,res) => {
  try {
    const userhistory = await UserGameHistory.findAll();
  
    res.json(userhistory);

  } catch (error) {
    res.json("Internal Error Server");
  }
});

// UPDATE
route.put("/history/:id", async (req,res) => {
  try {
    const { body } = req;
  
    const u_history = await UserGameHistory.update(body, {
      where: {
          id: req.params.id
      }
    });

    res.json(u_history);

  } catch (error) {
      res.json("Internal Error Server");
  }
});


// DELETE
route.delete("/history/:id", async(req,res) => {
  try {
    const d_history = await UserGameHistory.destroy({
        where: { 
            id: req.params.id
        }
    });
    
    res.json(d_history);
  
  } catch (error) {
    res.json("Internal Error Server");
  }
});


module.exports = route;

