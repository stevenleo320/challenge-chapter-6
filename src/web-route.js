const express = require("express");
const webRoute = express.Router();

const { UserGame, UserGameBiodata, UserGameHistory } = require("./models");

webRoute.get("/home", (req,res) => {
    try {

        res.render("home", {
            status : 0
        });

    } catch (error) {
        res.render("Internal Problem Server");
    }
});

webRoute.get("/game", (req,res) => {
    try {

        res.render("game", {
            status : 0
        });
    
    } catch (error) {
        res.render("Internal Problem Server");
    }
});

webRoute.get("/login", (req,res) => {
    try {
        res.render("login", {
            status : 200
        });
    } catch (error) {
        res.json("Internal Problem Server");
    }
});

webRoute.post("/login", async (req,res) => {

    const { username, password } = req.body;

    if(username && password) {

        const v_user = await UserGame.findOne({
            where: {
                username: req.body.username,
                password: req.body.password
            }
        });

        if(v_user.username === req.body.username && v_user.password === req.body.password) {
            const data = await UserGame.findAll({
                // where: {
                //     id : v_user.id
                // },
                include: [
                    {
                        model: UserGameBiodata
                    },
                    {
                        model: UserGameHistory
                    }
                ]
            });
            res.render("user/index",{
                data,
                number : 1,
                status : 200
            });
        } 
        else {
            res.render("login", {
                status : 500
            });
        }
    } else {
        res.render("login", {
            status : 412
        });
    }

});

// TAMPILAN AWAL SALES INDEX
webRoute.get("/user/index", async(req,res) => {

    const data = await UserGame.findAll({
        include: [
            {
                model: UserGameBiodata
            },
            {
                model: UserGameHistory
            }
        ]
    });
    res.render("user/index",{
        data,
        number : 1,
        status : 200
    });   
});

webRoute.get("/user/new", async(req,res) => {
    res.render("user/new",{
        status : 200 
    });    
});

// INSERT NEW USER GAME
webRoute.post("/register/admin", async (req,res) => {
    const { username, password } = req.body;

    if(username && password) {
        const i_data = await UserGame.create({username,password});

        res.redirect("/user/index");
    } else {
        res.render("user/new", {
            status : 500
        });
    }
});

// VIEW UPDATE
webRoute.get("/user/edit/:id", async (req,res) => {
    const data = await UserGame.findOne({
        where : { 
            id : req.params.id
        }
    });

    res.render("user/edit", {
        data
    });
});

// EDIT DATA USER 
webRoute.post("/user/edit/:id", async (req,res) => {
    const { body } = req;

    const data = await UserGame.update(body,{
        where : {
            id : req.params.id
        }
    });

    res.redirect("/user/index");
});

webRoute.get("/user/delete/:id",async (req,res) => {
    data_3 = await UserGameHistory.destroy({
        where: {
            userId : req.params.id
        }
    });

    if(data_3) {
        data_2 = await UserGameBiodata.destroy({
            where: {
                userId : req.params.id
            }
        });

        if(data_2) {
            data_1 = await UserGame.destroy({
                where: {
                    id : req.params.id
                }
            });
            
            res.redirect("/user/index");
        }
    }

});

webRoute.get("/user/detail/:id",async (req,res) => {
    biodata = await UserGameBiodata.findOne({
        where: {
            userId : req.params.id
        }
    });

    history = await UserGameHistory.findAll({
        where: {
            userId : req.params.id
        }
    });

    res.render("user/detail",{
        biodata,
        b_number : 1,
        history,
        h_number : 1
    });
});

module.exports = webRoute;


