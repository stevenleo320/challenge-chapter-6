## Project Structures 

- src(source)
-- config -> sequelize config
-- migrations -> Database migrations
-- models -> Table's models
-- seeders -> Data dummy creation
-- app.js -> app's middleware, etc
-- route.js -> app's route
-- web-route.js -> web route

## Model

Create User Game Model
```
npx sequelize-cli model:generate --name UserGame --attributes username:string,password:string
```

Create User Game Biodata Model
```
npx sequelize-cli model:generate --name UserGameBiodata --attributes name:string,email:string,country:string,userId:integer
```

Create User Game History Model
```
npx sequelize-cli model:generate --name UserGameHistory --attributes game_name:string,playing_time:integer,score:integer,userId:integer,biodataId:integer
```

## Migration

```
npx sequelize-cli db:create
```

```
npx sequelize-cli db:migrate
```

```
npx sequelize-cli db:migrate:undo
```

## Seeder

```
npx sequelize-cli db:seed:all
```

```
npx sequelize-cli db:seed:undo
```

## Web-route.js

- `GET` /home -> menampilkan view home.ejs

- `GET` /game -> menampilkan view game.ejs

- `GET` /login -> menampilkan login.ejs

- `POST` /login -> input data static untuk login

Login Form

```json
{
  "username": "username",
  "password": "password"
}
```

Response 

- `GET` /user/index -> menampilkan user/index.ejs yang berisikan data user game

Action Add New User

- `GET` /user/new -> menampilkan halaman user/new.ejs

- `POST` /register/admin -> input newUsername dan newPassword baru

```json
{
  "username": "newUsername",
  "password": "newPassword"
}
```

Action Update User Game 

- `GET` /user/edit/:id -> menampilkan page user/edit.ejs yang berisikan data user game berdasarkan `ID` yang di dapat

- `POST` /user/edit/:id -> mengupdate data user game dengan input editUsername dan editPassword

```json
{
  "username": "editUsername",
  "password": "editPassword"
}
```
Action Delete User Game

- `GET` /user/delete/:id -> menghapus data User Game History dan User Game Biodata yang memiliki userId yang sama dengan data User Game yang di delete.

Action Menampilkan Detail User Game 

- `GET` /user/detail/:id -> menampilkan data User Game History dan User Game Biodata yang memiliki userId yang sama dengan data yang dipilih untuk ditampilkan detail nya.











